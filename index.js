const got =require("../gameof_thrones/data");




// 1. Function to count total number of people
function countAllPeople() {
  let totalCount = 0;
  got.houses.forEach(house => {
    totalCount += house.people.length;
  });
  return totalCount;
}
console.log(countAllPeople());

// 2. Function to count total number of people in different houses
function peopleByHouses() {
  const peopleCountByHouse = {};
  got.houses.forEach(house => {
    peopleCountByHouse[house.name] = house.people.length;
  });
  return peopleCountByHouse;
}
console.log(peopleByHouses());


// 3. Function to get array of names of all people
function everyone() {
  const allPeople = [];
  got.houses.forEach(house => {
    house.people.forEach(person => {
      allPeople.push(person.name);
    });
  });
  return allPeople;
}
console.log(everyone());


// 4. Function to get array of names of people whose name includes 's' or 'S'
function nameWithS() {
  return everyone().filter(name => name.toLowerCase().includes('s'));
}

console.log(nameWithS());


// 5. Function to get array of names of people whose name includes 'a' or 'A'
function nameWithA() {
  return everyone().filter(name => name.toLowerCase().includes('a'));
}

console.log(nameWithA());


// // 6. Function to get array of names of people whose surname starts with 'S'


function surnameWithS() {
  return everyone().filter(name => {
    const splitName = name.split(' ');
    return splitName.length > 1 && splitName[1].startsWith('S');
  });
}

console.log(surnameWithS());


// // 7. Function to get array of names of people whose surname starts with 'A'


function surnameWithA() {
  return everyone().filter(name => {
    const splitName = name.split(' ');
    return splitName.length > 1 && splitName[1].startsWith('A');
  });
}


console.log(surnameWithA());


// 8. Function to get object with names of all people grouped by house
function peopleNameOfAllHouses() {
  const peopleByHouse = {};
  got.houses.forEach(house => {
    peopleByHouse[house.name] = house.people.map(person => person.name);
  });
  return peopleByHouse;
}

console.log(peopleNameOfAllHouses());



 